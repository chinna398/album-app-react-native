/**
 * @format
 */
//import Library
import React from 'react';
import {AppRegistry,View} from 'react-native';
import {name as appName} from './app.json';
import Header from './src/components/header';
import AlbumList from './src/components/AlbumList';

//Creating components
const App = () =>(
    <View style={{ flex: 1}}>
        <Header headerText={'Albums!'}/>
        <AlbumList />
    </View>
);


//Rendering the components to device
AppRegistry.registerComponent(appName, () => App);
